class ReportsController < ApplicationController
    def new #untuk menampilkan form data baru
      @report = Report.new
    end
  
    def create #untuk memproses data baru yang dimasukan di form new
      #render plain: params.inspect cara mengecek apa isi params
      #title = params [:book][:title] cara mngecek balikan params
      report = Report.new(resource_params)
      report.save 
      flash[:notice] = 'Report has been created'
      redirect_to reports_path
    end
  
    def edit #menampilkan data yang sudah disimpan di edit
      id = params[:id]
      @report = Report.find(id)
    end
  
    def update #melakuka prses ketika user mengedit data
      id = params[:id]
      @report = Report.find(params[:id])
      @report.update(resource_params)
      flash[:notice] = 'Report has been update'
        redirect_to reports_path(@report)
      
    end
  
    def destroy #untuk menghapus data
       id = params[:id]
       report = Report.find(params[:id])
       report.destroy
       flash[:notice] = 'Report has been delete'
       redirect_to reports_path
    end
  
    def index #menampilkan seluruh data yang ada di database
      @reports = Report.all
    end
  
    def show #menampilkan sebuah data secara detail
      id = params[:id]
      @report = Report.find (id)
      # render plain : id
      # render plain : @book.title
  
    end
  private
  def resource_params
    params.require(:report).permit(:title, :hasil, :mapel, :teacher_id, :student_id, :level)
  end
  end
  