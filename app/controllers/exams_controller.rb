class ExamsController < ApplicationController
    def new #untuk menampilkan form data baru
      @exam = Exam.new
    end
  
    def create #untuk memproses data baru yang dimasukan di form new
      #render plain: params.inspect cara mengecek apa isi params
      #title = params [:book][:title] cara mngecek balikan params
      exam = Exam.new(resource_params)
      exam.save 
      flash[:notice] = 'Exam has been created'
      redirect_to exams_path
    end
  
    def edit #menampilkan data yang sudah disimpan di edit
      id = params[:id]
      @exam = Exam.find(id)
    end
  
    def update #melakuka prses ketika user mengedit data
      id = params[:id]
      @exam = Exam.find(params[:id])
      @exam.update(resource_params)
      flash[:notice] = 'Exam has been update'
        redirect_to exams_path(@exam)
      
    end
  
    def destroy #untuk menghapus data
       id = params[:id]
       exam = Exam.find(params[:id])
       exam.destroy
       flash[:notice] = 'Exam has been delete'
       redirect_to exams_path
    end
  
    def index #menampilkan seluruh data yang ada di database
      @exams = Exam.all
    end
  
    def show #menampilkan sebuah data secara detail
      id = params[:id]
      @exam = Exam.find (id)
      # render plain : id
      # render plain : @book.title
  
    end
  private
  def resource_params
    params.require(:exam).permit(:title, :mapel, :duration, :nilai, :status, :level, :student_id)
  end
  end
  