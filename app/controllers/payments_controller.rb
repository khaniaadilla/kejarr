class PaymentsController < ApplicationController
    def new #untuk menampilkan form data baru
      @payment = Payment.new
    end
  
    def create #untuk memproses data baru yang dimasukan di form new
      #render plain: params.inspect cara mengecek apa isi params
      #title = params [:book][:title] cara mngecek balikan params
      payment = Payment.new(resource_params)
      payment.save 
      flash[:notice] = 'Payment has been created'
      redirect_to payments_path
    end
  
    def edit #menampilkan data yang sudah disimpan di edit
      id = params[:id]
      @payment = Payment.find(id)
    end
  
    def update #melakuka prses ketika user mengedit data
      id = params[:id]
      @payment = Payment.find(params[:id])
      @payment.update(resource_params)
      flash[:notice] = 'Payment has been update'
        redirect_to payments_path(@payment)
      
    end
  
    def destroy #untuk menghapus data
       id = params[:id]
       payment = Payment.find(params[:id])
       payment.destroy
       flash[:notice] = 'Payment has been delete'
       redirect_to payments_path
    end
  
    def index #menampilkan seluruh data yang ada di database
      @payments = Payment.all
    end
  
    def show #menampilkan sebuah data secara detail
      id = params[:id]
      @payment = Payment.find (id)
      # render plain : id
      # render plain : @book.title
  
    end
  private
  def resource_params
    params.require(:payment).permit(:id_transaction, :status, :upload)
  end
  end
  