class TeachersController < ApplicationController
    def new #untuk menampilkan form data baru
      @teacher = Teacher.new
    end
  
    def create #untuk memproses data baru yang dimasukan di form new
      #render plain: params.inspect cara mengecek apa isi params
      #title = params [:book][:title] cara mngecek balikan params
      teacher = Teacher.new(resource_params)
      teacher.save 
      flash[:notice] = 'Teacher has been created'
      redirect_to teachers_path
    end
  
    def edit #menampilkan data yang sudah disimpan di edit
      id = params[:id]
      @teacher = Teacher.find(id)
    end
  
    def update #melakuka prses ketika user mengedit data
      id = params[:id]
      @teacher = Teacher.find(params[:id])
      @teacher.update(resource_params)
      flash[:notice] = 'Teacher has been update'
        redirect_to teachers_path(@teacher)
      
    end
  
    def destroy #untuk menghapus data
       id = params[:id]
       teacher = Teacher.find(params[:id])
       teacher.destroy
       flash[:notice] = 'teacher has been delete'
       redirect_to teachers_path
    end
  
    def index #menampilkan seluruh data yang ada di database
      @teachers = Teacher.all
    end
  
    def show #menampilkan sebuah data secara detail
      id = params[:id]
      @teacher = Teacher.find (id)
      # render plain : id
      # render plain : @book.title
  
    end
  private
  def resource_params
    params.require(:teacher).permit(:nik, :nama, :age, :kelas, :mapel)
  end
  end
  