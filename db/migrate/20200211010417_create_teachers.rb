class CreateTeachers < ActiveRecord::Migration[6.0]
  def up
    create_table :teachers do |t|
      t.string :nik
      t.string :nama
      t.integer :age
      t.string :rombel
      t.string :mapel

      t.timestamps
    end
  end
  def def down 
    drop_table :teachers
  end
end
