class StudentsController < ApplicationController
  def new #untuk menampilkan form data baru
    @student = Student.new
  end

  def create #untuk memproses data baru yang dimasukan di form new
    #render plain: params.inspect cara mengecek apa isi params
    #title = params [:book][:title] cara mngecek balikan params
    student = Student.new(resource_params)
    student.save 
    flash[:notice] = 'Student has been created'
    redirect_to students_path
  end

  def edit #menampilkan data yang sudah disimpan di edit
    id = params[:id]
    @student = Student.find(id)
  end

  def update #melakuka prses ketika user mengedit data
    id = params[:id]
    @student = Student.find(params[:id])
    @student.update(resource_params)
    flash[:notice] = 'Student has been update'
      redirect_to students_path(@book)
    
  end

  def destroy #untuk menghapus data
     id = params[:id]
     student = Student.find(params[:id])
     student.destroy
     flash[:notice] = 'student has been delete'
     redirect_to students_path
  end

  def index #menampilkan seluruh data yang ada di database
    @students = Student.all
  end

  def show #menampilkan sebuah data secara detail
    id = params[:id]
    @student = Student.find (id)
    # render plain : id
    # render plain : @book.title

  end
private
def resource_params
  params.require(:student).permit(:name, :username, :age, :rombel, :address, :city, :nik)
end
end
